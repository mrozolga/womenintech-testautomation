from django.contrib import admin
from .models import SurveyAnswer

admin.site.register(SurveyAnswer)
