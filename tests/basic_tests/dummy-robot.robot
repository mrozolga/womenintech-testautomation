*** Settings ***
Documentation     Example using the space separated format.
Library           OperatingSystem
#Variables         dummy-variables.yaml

*** Variables ***
${MESSAGE}        Hello, world!
${BASIC_MESSAGE}        Hello, basic!


*** Test Cases ***
Basic Test
    Import Variables	${CURDIR}/dummy-variables.yaml
    Basic Keyword
    Log    ${FILE_MESSAGE}

First Test
    [Documentation]    Example test.
    Log    ${MESSAGE}
    My Keyword    ${CURDIR}

Next Test
    [Documentation]    Next example test.
    Log    ${MESSAGE}
    My Keyword with different variable ${CURDIR} passing

Last Test
    Should Be Equal    ${MESSAGE}    Hello, world!

*** Keywords ***
Basic Keyword
    Log    ${BASIC_MESSAGE}

My Keyword
    [Arguments]    ${path}
    Directory Should Exist    ${path}

My Keyword with different variable ${path} passing
    Directory Should Exist    ${path}